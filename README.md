# imgtotext

CLI tool to convert image files to text using python and tesseract.

## Prerequisites

- Tesseract installed, [check the docs](https://tesseract-ocr.github.io/tessdoc/) to install.
- Python >= 3.6

Note: These instructions are validated in Linux. 

## Installation

```bash
# 1. Clone to this repository and go to the repo dir
git clone https://gitlab.com/lbmendes/imgtotxt.git && cd imgtotxt

# 2. Create the virtual env for install the dependencies
python3 -m venv venv

# 3. Activate the virtual env
source venv/bin/activate

# 4. Install the requirements
pip install -r requirements.txt

# 5. Create the alias for CLI command
source create_alias.sh

# 6. Deactivate virtual env, change to other directory and test the imgtotxt command
deactivate
cd  # change to the home directory
imgtotxt --help
```


## Usage

Lets's say to you want to convert to text the image file ~/Downloads/df.jpg

![df.jpg - Image of a logo containing the text "DEAD FISH"](https://assets.betalabs.net/production/hsmerch/extra_fields/19/phpMztq4W1611242995.jpg)

So you do the command:
```bash
imgtotxt ~/Downloads/df.jpg
```

And will have the output:
```bash
List of params: [('image_file', PosixPath('/home/lucas/Downloads/df.jpg')), ('invert_color', False), ('grayscale', False), ('lang', None)]
************* Begin of text output  **************
DEAD FISH


*************** End of text output ***************

```

To improve the image analysis you coud use flags:
 - -i, --invert-color    -->  to invert colors, useful with dark background images 
 - -g, --grayscale       -->  to turn image to grayscale, sometimes help to get better results
 - -l LANG, --lang LANG  -->  to define the language to use. The langs availabe will be the langs installed in tesseract.

Using the help flag (-h, --help) you can check the option flags and the list of langs availabe, example:

```bash
imgtotxt [options] image_file

CLI tool to convert image files to text using tesseract

positional arguments:
  image_file            path to the image file to convert to text

options:
  -h, --help            show this help message and exit
  -i, --invert-color    invert colors, usefull with dark background images
  -g, --grayscale       turn image to grayscale, sometimes help to get better results
  -l LANG, --lang LANG  define the language to use, available options: 'eng', 'osd', 'por'
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
