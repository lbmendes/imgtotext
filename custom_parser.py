import argparse
import pathlib

from pytesseract import get_languages


def set_args():

    parser = argparse.ArgumentParser(
        description="CLI tool to convert image files to text using tesseract",
        usage="imgtotxt [options] image_file",
    )

    parser.add_argument(
        "image_file",
        help="path to the image file to convert to text",
        type=pathlib.Path,
    )

    parser.add_argument(
        "-i",
        "--invert-color",
        help="invert colors, useful with dark background images",
        action="store_true",
    )

    parser.add_argument(
        "-g",
        "--grayscale",
        help="turn image to grayscale, sometimes help to get better results",
        action="store_true",
    )

    langs = str(get_languages(config=""))[1:-1]
    parser.add_argument(
        "-l",
        "--lang",
        help=f"define the language to use, available options: {langs}",
    )

    return parser.parse_args()
